## Guacamole - client rdp for browser 

A basic configuration of guacamole:

Starting this container
    docker run -d -v /data/guacamole:/etc/guacamole -t aexoti/guacamole 


Persistent data configuration is stored in /etc/guacamole

